@extends('layouts.admin')
@section('title', 'Produtos')
@section('content')
<form class="form-horizontal form-bordered" id="form_cadastre" method="post"
    action="{{ isset($entity->id)?route('content.edit.save',['id'=>$entity->id]):route('content.save') }}">
    @csrf
    <input type="hidden" name="type" value="{{ $_type }}">
    <header class="page-header">
        <h2>Clientes</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="{{ route('information.index') }}">
                        <i class="fas fa-home"></i>
                    </a>
                </li>
                <li><span>Clientes</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open=""><i class="fas fa-chevron-left"></i></a>
        </div>
    </header>
    <section class="card">
        <header class="card-header">
            <div class="card-actions">
                <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
            </div>
            <h2 class="card-title">Cadastro de produto</h2>
            <p class="card-subtitle">
                Informações necessárias para a correta exibição.
            </p>
        </header>
        <div class="card-body" style="display: block;">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-align-left"></i>
                                    </span>
                                </span>
                                <input value="{{ isset($entity->title)!=""?$entity->title:'' }}" type="text"
                                    name="title" class="form-control" placeholder="Nome do produto" required>
                                {{-- <input value="{{ isset($entity->id)!=""?$entity->id:'' }}" type="text" name="id"
                                class="form-control ml-3" placeholder="Codigo" required> --}}
                            </div>
                        </div>
                        <div class="col-lg-12 mt-3">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-align-left"></i>
                                    </span>
                                </span>
                                <input value="{{ isset($entity->descripion)!=""?$entity->description:'' }}" type="text"
                                    name="description" style="height: 60px;" class="form-control"
                                    placeholder="Descrição" required>
                            </div>
                        </div>
                        <div class="col-lg-12 mt-3">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-align-left"></i>
                                    </span>
                                </span>
                                <input value="{{ isset($entity->short_description)!=""?$entity->short_description:'' }}"
                                    type="text" name="short_description" style="height: 80px;" class="form-control"
                                    placeholder="Informações do produto" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 control-label text-lg-right pt-2">Categorias</label>
                        <div class="col-lg-6">
                            <select name="categories[]" multiple data-plugin-selectTwo class="form-control populate"
                                id="select_categories">
                                @foreach ($categories as $category)
                                @if (isset($category->contents[0]->pivot->categories_id))
                                <option value="{{ $category->id }}"
                                    {{ $category->contents[0]->pivot->categories_id==$category->id ? 'selected' : "" }}>
                                    {{ $category->title }}</option>
                                    @else
                                    <option value="{{ $category->id }}">
                                        {{ $category->title }}</option>

                                {{-- <input name="categor" > --}}
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- <section class="card">
        <header class="card-header">
            <div class="card-actions">
                <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
            </div>
            <h2 class="card-title">Upload de arquivo</h2>
            <p class="card-subtitle">
                Selecione um arquivo para upload.<br />
                <br />
                <strong>Obs.: O arquivo selecionado tem que ser .pdf qualquer outro tipo não será carregado.</strong>
            </p>
        </header>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <form action="" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="upload_file" class="control-label col-sm-3">Upload File</label>
                            <div class="col-lg-12">
                                <input class="form-control" type="file" name="upload_file" id="upload_file">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section> --}}

    <section class="card">
        <header class="card-header">
            <div class="card-actions">
                <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
            </div>
            <h2 class="card-title">Imagem do produto</h2>
            <p class="card-subtitle">
                Selecione uma imagem como Imagem do produto.<br />
                <br />
                <strong>Obs.: A imagem selecionada será automatimacamente reajustada para o tamanho na descrição
                    do campo.</strong>
            </p>
        </header>
        <div class="card-body" style="display: block;">
            <div class="row">
                <div class="col-lg-12">
                    <small>Tamanho da imagem 280px x 280px</small>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="input-append">
                            <div class="uneditable-input">
                                <i class="fas fa-file fileupload-exists"></i>
                                <span class="fileupload-preview"></span>
                            </div>
                            <span class="btn btn-default btn-file">
                                <span class="fileupload-exists">Trocar</span>
                                <span class="fileupload-new">Selecionar Imagem</span>
                                <input type="file" name='image' accept="image/*" onchange='loadPreview(this, 280,280)'
                                    {{ isset($entity)?'':'required' }}>
                            </span>
                            <a href="forms-basic.html#" class="btn btn-default fileupload-exists" 167
                                data-dismiss="fileupload">Remove</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <textarea id="base64" name="base64" style="display:none;"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="img-content">
                        <img id='output' class='img-fluid'
                            src='{{ isset($entity->image)!=""?"/content/".$entity->id."/".$entity->image:'' }}'>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="card">
        <header class="card-header">
            <div class="card-actions">
                <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
            </div>
            <h2 class="card-title">Imagem do produto</h2>
            <p class="card-subtitle">
                Selecione uma imagem como Imagem do produto.<br />
                <br />
                <strong>Obs.: A imagem selecionada será automatimacamente reajustada para o tamanho na descrição
                    do campo.</strong>
            </p>
        </header>
        <div class="card-body" style="display: block;">
            <div class="row">
                <div class="col-lg-12">
                    <small>Tamanho da imagem 420px x 210px</small>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="input-append">
                            <div class="uneditable-input">
                                <i class="fas fa-file fileupload-exists"></i>
                                <span class="fileupload-preview"></span>
                            </div>
                            <span class="btn btn-default btn-file">
                                <span class="fileupload-exists">Trocar</span>
                                <span class="fileupload-new">Selecionar Imagem</span>
                                <input type="file" name='image' accept="image/*" onchange='loadPreview2(this, 420,210)'
                                    {{ isset($entity)?'':'required' }}>
                            </span>
                            <a href="forms-basic.html#" class="btn btn-default fileupload-exists" 167
                                data-dismiss="fileupload">Remove</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <textarea id="base64_2" name="base64_2" style="display:none;"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="img-content">
                        <img id='outputdois' class='img-fluid'
                            src='{{ isset($entity->image)!=""?"/contents/".$entity->id."/".$entity->image:'' }}'>
                    </div>
                </div>
            </div>
        </div>
    </section>


    {{-- 
    <section class="card">
        <header class="card-header">
            <div class="card-actions">
                <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
            </div>
            <h2 class="card-title">Informações nutricionais</h2>
            <p class="card-subtitle">
                Selecione uma imagem como Informações nutricionais.<br />
                <br />
                <strong>Obs.: A imagem selecionada será automatimacamente reajustada para o tamanho na descrição
                    do campo.</strong>
            </p>
        </header>
        <div class="card-body" style="display: block;">
            <div class="row">
                <div class="col-lg-12">
                    <small>Tamanho da imagem 1920px x 850px</small>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="input-append">
                            <div class="uneditable-input">
                                <i class="fas fa-file fileupload-exists"></i>
                                <span class="fileupload-preview"></span>
                            </div>
                            <span class="btn btn-default btn-file">
                                <span class="fileupload-exists">Trocar</span>
                                <span class="fileupload-new">Selecionar Imagem</span>
                                <input type="file" name='banner' accept="image/*" onchange='loadPreview(this, 1920,850)'
                                    {{ isset($entity)?'':'' }}>
    </span>
    <a href="forms-basic.html#" class="btn btn-default fileupload-exists" 167 data-dismiss="fileupload">Remove</a>
    </div>
    </div>
    </div>
    <div class="col-lg-12">
        <textarea id="base64" name="base64" style="display:none;"></textarea>
    </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="img-content">
                <img id='output' class='img-fluid'
                    src='{{ isset($entity->image)!=""?"/content/".$entity->id."/".$entity->image:'' }}'>
            </div>
        </div>
    </div>
    </div>
    </section> --}}

    <section class="card">
        <div class="card-body" style="display: block;">
            <button type="submit" class="mb-1 mt-1 mr-1 btn btn-success"><i class="fas fa-save"></i>
                Salvar</button>
        </div>
    </section>
</form>
</div>
</div>



<script id="delete_gallery_template" type="x-tmpl-mustache">
    <input type="text" name="delete_gallery[]" class="form-control input-sm" placeholder="" value=" "/>
</script>

<script>

</script>

<script id="thumb_template" type="x-tmpl-mustache">
    <div class="gallery_item">
        <img src="@{{ thumb }}" class="img-responsive"/>
        <input type="hidden" name="gallery_image[]" value="@{{ original }}" />
        <input type="hidden" name="gallery_image_thumb[]" value="@{{ thumb }}" />
        <input type="text" name="gallery_image_descriptioreadAll[]" class="form-control input-sm" placeholder="Descrição da imagem"/>
        <a href="javascript:;" class="" id="gallery_item_remove">
            <i class="mdi mdi-delete"></i>Remover
        </a>
    </div>
</script>

@endsection