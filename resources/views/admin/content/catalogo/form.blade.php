@extends('layouts.admin')
@section('title', 'Upload de catalogo')
@section('content')
<form class="form-horizontal form-bordered" id="form_cadastre" method="post" enctype="multipart/form-data"
    action="{{ route('content.save.file') }}">
    @csrf

    {{-- <form action="{{route('upload')}}" class="form-horizontal form-bordered" id="form_cadastre" method="POST"
    enctype="multipart/form-data"> --}}
    <input type="hidden" name="type" value="{{ $_type }}">
    <header class="page-header">
        <h2>Catalogo</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="{{ route('information.index') }}">
                        <i class="fas fa-home"></i>
                    </a>
                </li>
                <li><span>Catalogo</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open=""><i class="fas fa-chevron-left"></i></a>
        </div>
    </header>
    <section class="card">
        <header class="card-header">
            <div class="card-actions">
                <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
            </div>
            <h2 class="card-title">Upload de catalogo</h2>
            <p class="card-subtitle">
                Informações necessárias para a correta exibição.
            </p>
        </header>
        <div class="card-body" style="display: block;">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-align-left"></i>
                                    </span>
                                </span>
                                <input value="{{isset($entity->title)!=""?$entity->title:''}}" type="text" name="title"
                                    class="form-control" placeholder="Nome do produto" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="card">
        <header class="card-header">
            <div class="card-actions">
                <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
            </div>
            <h2 class="card-title">PDF</h2>
            <p class="card-subtitle">
                Selecione um arquivo PDF.<br />
                <br />
                <strong>Obs.: Selecionar somentes arquivos PDF.</strong>
            </p>
        </header>
        <div class="card-body" style="display: block;">
            <div class="row">
                <div class="col-lg-12">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="container">
                            {{-- <input type="button" value="+" id="add-file-btn" class="form-control"> --}}
                            <input type='file' accept="application/pdf" name="pdf_file" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="card">
        <div class="card-body" style="display: block;">
            <button type="submit" class="mb-1 mt-1 mr-1 btn btn-success"><i class="fas fa-save"></i>
                Salvar</button>
        </div>
    </section>
    </div>
</form>

<script id="delete_gallery_template" type="x-tmpl-mustache">
    <input type="text" name="delete_gallery[]" class="form-control input-sm" placeholder="" value=" "/>
</script>
<script id="thumb_template" type="x-tmpl-mustache">
    <div class="gallery_item">
        <img src="@{{ thumb }}" class="img-responsive"/>
        <input type="hidden" name="gallery_image[]" value="@{{ original }}" />
        <input type="hidden" name="gallery_image_thumb[]" value="@{{ thumb }}" />
        <input type="text" name="gallery_image_descriptioreadAll[]" class="form-control input-sm" placeholder="Descrição da imagem"/>
        <a href="javascript:;" class="" id="gallery_item_remove">
            <i class="mdi mdi-delete"></i>Remover
        </a>
    </div>
</script>


@endsection