@extends('layouts.default')

@section('title')
    Qualidade | Kadão Alimentos
@endsection

@section('content')
<section class="interna-qualidade">
    <div class="continer h-100">
        <div class="row m-0 h-100">
            <div class="my-auto font-white col-sm-5 offset-sm-7">
                <div class="items-content">
                    <h3 class="font-orange">
                        A QUALIDADE KADÃO
                    </h3>
                    <p class="w-85">
                        Além do controle rigoroso de processos para oferecer os melhores produtos, a Kadão Alimentos
                        investe na integração entre colaboradores e conta com vários programas de controle de qualidade.
                    </p>
                </div>
                <div class="items-content">
                    <h3 class="font-orange">
                        Origem certificada de carnes
                    </h3>
                    <ul class="pl-4">
                        <li class="w-60">
                            Laboratório próprio para análises físicas, químicas e microbiológicas;
                        </li>
                        <li class="w-60">
                            Certificações de exportação;
                        </li>
                    </ul>
                </div>
                <div class="items-content">
                    <h3 class="font-orange">
                        Certificação
                    </h3>
                    <p class="w-60">BPF: Boas Práticas de Fabricação</p>
                    <p class="w-60">PSO: Procedimento Sanitário Operacional</p>
                    <p class="w-60">PPHO: Procedimento Padrão de Higiene Operacional</p>
                    <p class="w-60">POP: Procedimento Operacional Padrão</p>
                    <p class="w-60">APPCC: Análise de Perigo e Pontos Críticos de Controle</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection