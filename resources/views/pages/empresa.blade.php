@extends('layouts.default')

@section('title')
    Empresa | Kadão Alimentos
@endsection

@section('content')

<div class="interna-empresa container">
    <div class="row">
        <div class="col-sm-8 mt-5">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="font-orange">
                        A KADÃO ALIMENTOS
                    </h2>
                    <p>A Kadão Alimentos nasceu em 2010, após um longo período de atuação de seus fundadores no
                        agronegócio. Motivados pelo interesse em processamento de carne bovina, a empresa tem marcado
                        seu nome neste mercado com produtos de alta qualidade, controle rigoroso de processos e a busca
                        constante pela total satisfação de seus clientes. Com uma administração familiar, a Kadão sabe o
                        que o brasileiro deseja colocar em sua mesa: um alimento de ótimo custo-benefício, com garantia
                        de qualidade e procedência.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <h4 class="font-orange">Missão</h4>
                    <p>
                        Fortalecer a indústria brasileira de alimentos e toda a sua cadeia produtiva, desenvolvendo
                        produtos e serviços de alta qualidade que enfatizem cada vez mais parcerias comerciais
                        socialmente, economicamente e ambientalmente responsáveis.
                    </p>
                </div>
                <div class="col-sm-12 col-md-4">
                    <h4 class="font-orange">Visão</h4>
                    <p>
                        A Kadão será uma indústria referência no ramo de carnes de alta qualidade, com presença forte no
                        mercado nacional e internacional, levando as melhores soluções em alimentação para todos os
                        clientes.
                    </p>
                </div>
                <div class="col-sm-12 col-md-4">
                    <h4 class="font-orange">Valores</h4>
                    <ul class="ml-4">
                        <li>Transparência e ética</li>
                        <li>Qualidade e inovação</li>
                        <li>Determinação na busca constante dos melhores resultados</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <img src="{{asset('img/img-1.jpg')}}" class="img-fluid mt-5">
            <img src="{{asset('img/img-2.jpg')}}" class="img-fluid mt-3">
        </div>
    </div>
</div>
<div class="container-fluid mt-5">
    <div class="row">
        <img src="{{asset('img/bg-empresa.jpg')}}" alt="" class="img-fluid">
    </div>
</div>

@endsection