@extends('layouts.default')

@section('title')
    Presença | Kadão Alimentos
@endsection

@section('content')
<section class="interna-presenca">
    <div class="container h-100">
        <div class="row h-100">
            <div class="my-auto col-md-5">
                <h2 class="font-orange pb-3">A PRESENÇA KADÃO</h2>
                <div class="items-content py-4">
                    <h3 class="font-blue">
                        EXPORTAÇÃO
                    </h3>
                    <p>
                        Os produtos Kadão estão na mesa não apenas de brasileiros, mas também em países do Oriente
                        Médio, Angola e Hong Kong. Com excelente custo-benefício e cada vez mais interesse do mercado
                        externo nos produtos Kadão, a empresa aumenta gradativamente sua participação nas vendas
                        internacionais.
                    </p>
                </div>
                <div class="items-content py-4">
                    <h3 class="font-blue">
                        IMPORTAÇÃO
                    </h3>
                    <ul class="flags d-flex">
                        <li class="text-center p-3">
                            <img src="{{asset('img/argentina.png')}}" class="d-block">
                            <span class="font-orange">Argentina</span>
                        </li>
                        <li class="text-center p-3">
                            <img src="{{asset('img/paraguai.png')}}" class="d-block">
                            <span class="font-orange">Paraguai</span>
                        </li>
                        <li class="text-center p-3">
                            <img src="{{asset('img/uruguai.png')}}" class="d-block">
                            <span class="font-orange">Uruguai</span>
                        </li>
                    </ul>
                    <p>
                        Além da carne produzida em vários estados do Brasil, a Kadão também importa de alguns países da
                        América do Sul, com rigoroso controle de qualidade e procedência.
                    </p>
                </div>
                <div class="items-content py-4">
                    <h3 class="font-blue">
                        MERCADO INTERNO
                    </h3>
                    <p>
                        A Kadão está presente em praticamente todas as regiões do Brasil com suas linhas de produtos. A
                        cada ano, o número de clientes aumenta e a empresa amplia sua capacidade de atendimento.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection