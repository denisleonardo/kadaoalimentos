@extends('layouts.default')

@section('title')
    Produtos | Kadão Alimentos
@endsection

@section('content')
<section class="interna-produtos">
    <div class="container p-0">
        <div class="row m-0">
            <div class="text-center col-md-4 offset-md-4 p-0 animated fadeIn">
                <h1 class="font-brow">
                    EXPERIMENTE
                </h1>
                <p class="font-white py-3">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce consequat orci sed erat interdum, sed
                    pulvinar eros ultrices. Donec ac felis mattis, laoreet magna eu, pellentesque neque.
                </p>
            </div>
        </div>
        <div class="row p-0 m-0">
            @foreach ($produto as $item)
            <div class="col-lg-6 p-0 content-box mb-5 animated fadeInUp">
                <a href="{{route('produto', ['url'=> $item->url ])}}">
                    <div class="produto-item">
                        <img src="{{asset('cat/'.$item->id.'/'.$item->image)}}" class="produto-imagem img-fluid">
                        <div class="produto-item-descricao">{{$item->title}}</div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection