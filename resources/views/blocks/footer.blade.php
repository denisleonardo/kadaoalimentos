<footer>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="text-center my-auto col-sm-4">
                    <a href="mailto:contato@kadaoalimentos.com.br" class="text-decoration-none">
                        <img src="{{asset('img/icon-mail.png')}}" class="img-fluid mb-2">
                        <p class="font-white">contato@kadaoalimentos.com.br</p>
                    </a>
                </div>
                <div class="text-center my-auto col-sm-4">
                    <img src="{{asset('img/icon-kadao.png')}}" class="img-fluid">
                </div>
                <div class="text-center col-sm-4 mt-3">
                    <img src="{{asset('img/icon-map.png')}}" alt="" class="img-fluid mb-2">
                    <p class="font-white">
                        Rodovia GO 206 - Km 01 - s/n
                        <br>
                        Zona Rural (307,95 km)
                        <br>
                        75813000 Caçu, Goias, Brazil
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-down">
        <div class="container">
            <div class="row">
                <div class="text-center col-sm-12">
                    © Copyright 2018 - 2019. Todos os Direitos Reservados a Kadão Alimentos. Desenvolvido por <a href="#" class="font-8">LED Digital</a>
                </div>
            </div>
        </div>
    </div>
</footer>