$(document).ready(function () {
    $('#slider').nivoSlider({ 
        effect: 'random', 
        slices: 15, 
        boxCols: 8, 
        boxRows: 4, 
        animSpeed: 1000, 
        pauseTime: 5000, 
        startSlide: 0, 
        directionNav: true, 
        controlNav: false, 
        controlNavThumbs: false, 
        pauseOnHover: false, 
        manualAdvance: false, 
        prevText: 'Prev', 
        nextText: 'Next', 
        randomStart: false, 
        beforeChange: function () { }, 
        afterChange: function () { }, 
        slideshowEnd: function () { }, 
        lastSlide: function () { }, 
        afterLoad: function () { } 
    });
});