<?php

namespace App\Http\Controllers;

use App\Categories;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Categories();
    }

    public function index(Request $request)
    {
        return view(
            'admin.categories.index'
        );
    }

    //Listar todas as categorias
    public function readAll(Request $request)
    {
        $collection = $this->model->get()->all();
        $data['data'] = $collection;
        echo json_encode($data);
    }


    public function form(Request $request)
    {
        $id = $request->route('id');
        if (isset($id) and ($id != "")) {
            $entity = $this->model->find($id);
            return view('admin.categories.form', ['entity' => $entity]);
        } else {
            return view('admin.categories.form');
        }
    }

    public function save(Request $request)
    {
        $folder = public_path() . '/cat/';
        $form = $request->all();
        $id = $request->route('id');

        //Verifica se possui algum registro
        if (!isset($id) and $id == "") {

            //Validação de URL
            $form['url'] = $this->url_verify($form['title'], $this->model);

            //Fazer inserção do produto
            if ($entity = $this->model->create($form)) {

                //Cria a pasta de acordo com o conteudo criado
                $folder_content = $folder . '/' . $entity->id . '/'; //Pasta referencia ao conteudo criado
                $folder_gallery = $folder_content . '/gallery/'; //Pasta da Galeria
                $folder_thumb = $folder_content . '/thumb/'; // Pasta da  MIniatura de Galeria

                //Cria todas as pastas para o conteudo
                if (!file_exists($folder_content)) {
                    if (mkdir($folder_content, 0777)) { //Cria a pasta inicial
                        if (mkdir($folder_gallery, 0777)) { //Cria a pasta da galeria
                            mkdir($folder_thumb, 0777); // Cria a pasta de Miniaturas
                        }
                    }
                }

                //Gera um novo nome de arquivo
                if (isset($form['base64']) and ($form['base64'] != "")) {
                    //Função para salvar e pegar o nome do arquivo de imagem
                    $image_name = $this->get_image_base64($form['base64'], function ($img, $name) use ($folder_content) {
                        file_put_contents($folder_content . $name, $img);
                        return $name;
                    });
                    $update['image'] = $image_name;
                    $entity->update($update);
                }

                //Cadastro de fotos na galeria de imagens
                if (isset($form['gallery_image'])) {
                    foreach ($form['gallery_image'] as $key => $image) {

                        $image_description = $form['gallery_image_description'][$key]; //Pega o valor do array da descricao da imagem da galeria
                        $image_thumb = $form['gallery_image_thumb'][$key]; //Pega o valor do array da miniatura da imagem

                        //Salva as imagens principais
                        $image_name_principal = $this->get_image_base64($image, function ($img, $name) use ($folder_gallery) {
                            file_put_contents($folder_gallery . $name, $img);
                            return $name;
                        });

                        //Salva as imagens miniaturas
                        $image_name_thumb = $this->get_image_base64($image_thumb, function ($img, $name) use ($folder_thumb, $image_name_principal) {
                            file_put_contents($folder_thumb . $image_name_principal, $img);
                            return $image_name_principal;
                        });

                        //Informaçoes necessárias para inserçao
                        $image_data = [
                            'description' => $image_description,
                            'type' => 'gallery',
                            'order' => '0',
                            'image' => $image_name_principal,
                            'path' => $folder_content,
                        ];

                        $entity->images()->create($image_data);
                    }
                }

                $res = [
                    'status' => 200,
                    'data' => $entity
                ];
            } else {
                //Fazer update do registro
                $entity = $this->model->find($id);
                //Validação de URL
                $form['url'] = $this->url_verify($form['title'], $this->model, $entity->id);

                $entity = $entity->update($form);
                $res = [
                    'status' => 200,
                    'data' => $entity
                ];
            }
            return response()->json($res);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->route('id');
        $entity = $this->model->find($id);
        $entity->delete();
    }
}
